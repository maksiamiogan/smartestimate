from flask_login import UserMixin

from sweater import db_b, manager


class User(db_b.Model, UserMixin):
    id = db_b.Column(db_b.Integer, primary_key=True)
    login = db_b.Column(db_b.String(128), nullable=False, unique=True)
    password = db_b.Column(db_b.String(255), nullable=False)
    is_admin = db_b.Column(db_b.Boolean, nullable=False, unique=False, default=False)

@manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)
