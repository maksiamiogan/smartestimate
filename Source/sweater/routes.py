from flask import render_template, redirect, url_for, request, flash
from flask_login import login_user, login_required, logout_user, current_user
from werkzeug.security import check_password_hash, generate_password_hash
from werkzeug.urls import url_parse

from sweater import app, db_b, db
from sweater.models import User
import numpy as np
import sqlite3
from vectorizer import vect

from wtforms import Form, TextAreaField, validators
from sweater import clf


class ReviewForm(Form):
    moviereview = TextAreaField('', [validators.DataRequired(), validators.length(min=15)])
    movietitle = TextAreaField('', [validators.DataRequired(), validators.length(min=1)])


class c_ReviewForm(Form):
    moviereview = TextAreaField('', [validators.DataRequired(), validators.length(min=15)])


def classify(document):
    label = {0: 'negative', 1: 'positive'}
    X = vect.transform([document])
    y = clf.predict(X)[0]
    proba = np.max(clf.predict_proba(X))
    return label[y], proba


def train(document, y):
    X = vect.transform([document])
    clf.partial_fit(X, [y])


def sqlite_entry(path, title, document, y):
    conn = sqlite3.connect(db)
    c = conn.cursor()
    cursor = c.execute('SELECT id FROM movies_db WHERE title = ?', (title,))
    movie_id = cursor.fetchall()
    movie_id = movie_id[0][0]
    c.close()
    conn.close()
    conn = sqlite3.connect(path)
    c = conn.cursor()
    c.execute("INSERT INTO review_db (title, review, sentiment, date, movie_id, user_id) VALUES (?, ?, ?, DATETIME('now'), ?, ?)",
              (title, document, y, movie_id, current_user.id))
    conn.commit()
    conn.close()


def sqlite_entry_genres(path, g):
    conn = sqlite3.connect(path)
    c = conn.cursor()
    c.execute("UPDATE genres_db SET amount = amount + 1 WHERE genre = ?", (g,))
    conn.commit()
    c.close()
    conn.close()


def sqlite_entry_title(path, title, genre):
    conn = sqlite3.connect(path)
    c = conn.cursor()
    c.execute("INSERT INTO movies_db (title, genre) VALUES (?, ?)", (title, genre))
    c.execute("UPDATE movies_db SET comm_count = comm_count + 1 WHERE title = ?", (title,))
    conn.commit()
    c.close()
    conn.close()


def sqlite_entry_title_score(path, title, sc):
    conn = sqlite3.connect(path)
    c = conn.cursor()
    c.execute("UPDATE movies_db SET score = score + ? WHERE title = ?", (sc, title))
    conn.commit()
    c.close()
    conn.close()


@app.route('/')
def main():
    conn = sqlite3.connect(db)
    c = conn.cursor()
    cursor = c.execute('SELECT * FROM movies_db')
    items = cursor.fetchall()
    c.close()
    conn.close()
    return render_template('main.html', items=items)

@app.route('/about')
def about_page():
    return render_template('about.html')


@app.route('/users')
def users_db():
    conn = sqlite3.connect(db)
    c = conn.cursor()
    cursor = c.execute('SELECT id FROM user')
    users = cursor.fetchall()
    c.close()
    conn.close()
    return render_template('user_list.html', users=users)


@app.route('/delete_movie', methods=['POST'])
@login_required
def del_movie():
    title = request.form['delete']
    print(title)
    conn = sqlite3.connect(db)
    c = conn.cursor()
    c.execute('DELETE FROM movies_db WHERE title = ?', (title,))
    conn.commit()
    c.close()
    conn.close()
    conn = sqlite3.connect(db)
    c = conn.cursor()
    cursor = c.execute('SELECT * FROM movies_db')
    items = cursor.fetchall()
    c.close()
    conn.close()
    return render_template('main.html', items=items)


@app.route('/delete_user', methods=['POST'])
@login_required
def del_user():
    id = request.form['delete']
    conn = sqlite3.connect(db)
    c = conn.cursor()
    c.execute('DELETE FROM user WHERE id = ?', (id,))
    conn.commit()
    c.close()
    conn.close()
    conn = sqlite3.connect(db)
    c = conn.cursor()
    cursor = c.execute('SELECT id FROM user')
    users = cursor.fetchall()
    c.close()
    conn.close()
    return render_template('user_list.html', users=users)


@app.route('/delete_review', methods=['POST'])
@login_required
def del_rev():
    id = request.form['delete']
    title = request.form['title']
    conn = sqlite3.connect(db)
    c = conn.cursor()
    c.execute('DELETE FROM review_db WHERE id = ?', (id,))
    conn.commit()
    c.close()
    conn.close()
    conn = sqlite3.connect(db)
    c = conn.cursor()
    c.execute("UPDATE movies_db SET comm_count = comm_count - 1 WHERE title = ?", (title,))
    conn.commit()
    c.close()
    conn.close()
    return redirect(url_for('main'))


@app.route('/estimate')
@login_required
def estimate():
    return render_template('genres_choose.html')


@app.route('/certain_estimate', methods=['POST'])
@login_required
def c_estimate():
    title = request.form['c_estimate']
    form = ReviewForm(request.form)
    return render_template('c_est.html', form=form, title=title)


@app.route('/index', methods=['POST'])
@login_required
def index():
    form = ReviewForm(request.form)
    genres = request.form['genre']
    return render_template('reviewform.html', form=form, genre=genres)


@app.route('/results', methods=['POST'])
@login_required
def results():
    form = ReviewForm(request.form)
    if request.method == "POST" and form.validate():
        conn = sqlite3.connect(db)
        c = conn.cursor()
        cursor = c.execute('SELECT title FROM movies_db')
        titles = cursor.fetchall()
        c.close()
        review = request.form['moviereview']
        title = request.form['movietitle']
        genres = request.form['genre']
        print(titles)
        if titles:
            if title in titles[0]:
                print("LOOOOOOOOOOOOOOOL")
                flash('That film already exist in database')
                return render_template('reviewform.html', form=form)
        y, proba = classify(review)
        sqlite_entry_genres(db, genres)
        sqlite_entry_title(db, title, genres)
        return render_template('results.html', content=review, prediction=y, probability=round(proba * 100, 2),
                               title=title)
    return render_template('reviewform.html', form=form)


@app.route('/certain_results', methods=['POST'])
@login_required
def c_results():
    form = c_ReviewForm(request.form)
    if request.method == "POST" and form.validate():
        review = request.form['moviereview']
        title = request.form['title']
        genres = request.form['genre']
        y, proba = classify(review)
        conn = sqlite3.connect(db)
        c = conn.cursor()
        c.execute("UPDATE movies_db SET comm_count = comm_count + 1 WHERE title = ?", (title,))
        conn.commit()
        c.close()
        conn.close()
        return render_template('results.html', content=review, prediction=y, probability=round(proba * 100, 2),
                               title=title)
    title = request.form['title']
    return render_template('c_est.html', form=form, title=title)


@app.route('/thanks', methods=['POST'])
@login_required
def feedback():
    feedback_in = request.form['feedback_button']
    review = request.form['review']
    prediction = request.form['prediction']
    title = request.form['title']
    inv_label = {'negative': 0, 'positive': 1}
    y = inv_label[prediction]
    inv_label = {'negative': -1, 'positive': 1}
    sc = inv_label[prediction]
    print(y)
    if feedback_in == 'Incorrect':
        y = int(not (y))
        sc = sc * -1

    train(review, y)
    sqlite_entry(db, title, review, y)
    sqlite_entry_title_score(db, title, sc)
    return render_template('thanks.html')


@app.route('/movie', methods=['POST'])
def movie():
    title = request.form['title']
    conn = sqlite3.connect(db)
    c = conn.cursor()
    cursor = c.execute('SELECT * FROM review_db WHERE title = ?', (title,))
    comments = cursor.fetchall()
    c.close()
    conn.close()
    conn = sqlite3.connect(db)
    c = conn.cursor()
    cursor = c.execute('SELECT * FROM movies_db WHERE title = ?', (title,))
    info = cursor.fetchall()
    c.close()
    conn.close()
    return render_template('movie.html', title=title, comm=comments, info=info)


@app.route('/login', methods=['GET', 'POST'])
def login_page():
    login = request.form.get('login')
    password = request.form.get('password')

    if login and password:
        user = User.query.filter_by(login=login).first()

        if user and check_password_hash(user.password, password):
            login_user(user)
            next_page = request.args.get('next')
            print(next_page)
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('main')
            return redirect(next_page)
        else:
            flash('Login or password is not correct')
    else:
        flash('Please fill login and password fields')

    return render_template('login.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    login = request.form.get('login')
    password = request.form.get('password')
    password2 = request.form.get('password2')

    if request.method == 'POST':
        if not (login or password or password2):
            flash('Please, fill all fields!')
        elif password != password2:
            flash('Passwords are not equal!')
        else:
            print(password)
            hash_pwd = generate_password_hash(password)
            new_user = User(login=login, password=hash_pwd)
            db_b.session.add(new_user)
            db_b.session.commit()

            return redirect(url_for('login_page'))

    return render_template('register.html')


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('main'))


@app.after_request
def redirect_to_signin(response):
    if response.status_code == 405 or response.status_code == 401:
        print(response)
        return redirect(url_for('login_page') + '?next=' + request.url)
    print(response)
    return response
