from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import pickle

import os

app = Flask(__name__)
app.secret_key = 'some secret salt'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db/reviews.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db_b = SQLAlchemy(app)
manager = LoginManager(app)

# Подготовка классификатора
cur_dir = os.path.dirname(__file__)
clf = pickle.load(open(os.path.join(cur_dir, '../pkl_objects', 'classifier.pkl'), 'rb'))
db = os.path.join(cur_dir, 'db/reviews.db')

from sweater import models, routes

db_b.create_all()
