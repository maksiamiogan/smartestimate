## Список интерфейсов ##

1. Навигационная панель
2. Форма регистрации
3. Форма входа
4. Таблица фильмов 
5. Страница для публикации отзыва
6. Форма выбора жанра
7. Форма проверки корректности предсказания
8. Интерфейс удаления фильмов
9. Интерфейс удаление комментариев
10. Интерфейс удаления пользователей

## Эскизы интерфейсов ##

![Эскизы пользователя](https://gitlab.com/maksiamiogan/smartestimate/-/blob/main/Scatch.png)

![Эскизы администратора](https://gitlab.com/maksiamiogan/smartestimate/-/blob/main/AdminScatch.png)

## Диаграмма интерфейсов ##

![Диаграмма интерфейсов](https://gitlab.com/maksiamiogan/smartestimate/-/blob/main/Inter_diagr.jpeg)
